print("Importing mlisp.sml from hw1\n");
use "..\\pl-hw1\\mlisp.sml";
print("Importing mlisp.sml from hw2\n");
use "..\\pl-hw2\\mlisp.sml";
print("Importing mlisp.sml from hw4\n");
use "..\\pl-hw4\\mlisp.sml";

print("Importing mlisp.sml from hw5\n");

exception MlispError;

local
  fun isListable (ATOM(NIL)) = true
    | isListable (CONS(_, rSexp)) = isListable rSexp
    | isListable _ = false
in
  fun eval (ATOM(NUMBER n)) env = (ATOM(NUMBER n), env)
    | eval (ATOM(NIL)) env = (ATOM(NIL), env)
    | eval (ATOM(SYMBOL s)) env = ((find s env) handle Undefined => raise MlispError, env)
    | eval sexp env =
      let
        fun invokeUserDefinedFunc fName actualParams env =
          let
            fun getFormalParams (CONS(params, _)) = params
              | getFormalParams _ = raise MlispError

            fun getFuncBody (CONS(_, CONS(body, ATOM(NIL)))) = body
              | getFuncBody _ = raise MlispError

            fun addParamsBindings (ATOM(NIL)) (ATOM(NIL)) singleEnv env = singleEnv
              | addParamsBindings (CONS(ATOM(SYMBOL(formal)), formalLeft)) (CONS(actual, actualLeft)) singleEnv env =
                  let
                    val (actualValue, actualNewEnv) = eval actual env
                  in
                    addParamsBindings formalLeft actualValue (define formal singleEnv actual) actualNewEnv
                  end
              | addParamsBindings _ _ _ _ = raise MlispError 

            val funcDef = find fName env
            val formalParams = getFormalParams funcDef
            val funcBody = getFuncBody funcDef
            val envWithParamsBindings = addParamsBindings formalParams actualParams (initEnv()) env
            val nestedEnvWithParamsBindings = pushEnv envWithParamsBindings env
            val evalWithFunctionEnv = eval funcBody nestedEnvWithParamsBindings
            val (outputSexp, newEnv) = evalWithFunctionEnv
          in
            (outputSexp, popEnv newEnv)
          end

        fun plusFunc (CONS(first, CONS(second, ATOM(NIL)))) env =
          let
            fun obtainIntParam (ATOM(NUMBER(x)), _) = x
              | obtainIntParam _ = raise MlispError

            val x = obtainIntParam (eval first env)
            val y = obtainIntParam (eval second env)
          in
            (ATOM(NUMBER(x + y)), env)
          end
          | plusFunc _ _ = raise MlispError
        
        fun minusFunc (CONS(first, CONS(second, ATOM(NIL)))) env =
          let
            fun obtainIntParam (ATOM(NUMBER(x)), _) = x
              | obtainIntParam _ = raise MlispError

            val x = obtainIntParam (eval first env)
            val y = obtainIntParam (eval second env)
          in
            (ATOM(NUMBER(x - y)), env)
          end
          | minusFunc _ _ = raise MlispError

        fun multFunc (CONS(first, CONS(second, ATOM(NIL)))) env =
          let
            fun obtainIntParam (ATOM(NUMBER(x)), _) = x
              | obtainIntParam _ = raise MlispError

            val x = obtainIntParam (eval first env)
            val y = obtainIntParam (eval second env)
          in
            (ATOM(NUMBER(x * y)), env)
          end
          | multFunc _ _ = raise MlispError
        
        fun divFunc (CONS(first, CONS(second, ATOM(NIL)))) env =
          let
            fun obtainIntParam (ATOM(NUMBER(x)), _) = x
              | obtainIntParam _ = raise MlispError

            val x = obtainIntParam (eval first env)
            val y = obtainIntParam (eval second env)
          in
            if y = 0 then raise MlispError else (ATOM(NUMBER(x div y)), env)
          end
          | divFunc _ _ = raise MlispError

        fun consFunc (CONS(first, CONS(second, ATOM(NIL)))) env =
          let
            val (firstEval, _) = eval first env
            val (secondEval, _) = eval second env 
          in
            (CONS(firstEval, secondEval), env)
          end
          | consFunc _ _ = raise MlispError

        fun carFunc (CONS(list, _)) env =
          let
            val (listEval, _) = eval list env
            val (CONS(head, _)) = listEval
          in
            (head, env)
          end
          | carFunc _ _ = raise MlispError
        
        fun cdrFunc (CONS(list, _)) env =
          let
            val (listEval, _) = eval list env
            val (CONS(_, tail)) = listEval
          in
            (tail, env)
          end
          | cdrFunc _ _ = raise MlispError

        fun defineFunc (CONS(name, CONS(value, ATOM(NIL)))) env =
          let
            val (ATOM(SYMBOL(nameString))) = name
            val (valueEval, _) = eval value env
          in
            (ATOM(NIL), defineNested nameString env valueEval)
          end

        fun invokePredefinedFunc fName actualParams env =
          case fName of
             "+" => plusFunc actualParams env
           | "-" => minusFunc actualParams env
           | "*" => multFunc actualParams env
           | "div" => divFunc actualParams env
           | "cons" => consFunc actualParams env
           | "car" => carFunc actualParams env
           | "cdr" => cdrFunc actualParams env
           | "define" => defineFunc actualParams env
           | _ => raise MlispError
        
        fun invokeFunc fName actualParams env =
          let
            val funcDef = (find fName env) handle Undefined => ATOM(NIL)
          in
            if funcDef <> ATOM(NIL) then
              invokeUserDefinedFunc fName actualParams env
            else invokePredefinedFunc fName actualParams env
          end

        fun evalList (CONS(ATOM(SYMBOL(fName)), params)) env = invokeFunc fName params env
          | evalList _ _ = raise MlispError
      in
        if isListable sexp then (evalList sexp env) else raise MlispError
      end
end;
