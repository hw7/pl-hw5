datatype 'a seq = Nil | Cons of 'a * (unit-> 'a seq);

exception EmptySeq;
fun head(Cons(x,_)) = x | head Nil = raise EmptySeq;
fun tail(Cons(_,xf)) = xf() | tail Nil = raise EmptySeq;


datatype direction = Back | Forward;
datatype 'a bseq =   bNil
		        | bCons of 'a * (direction -> 'a bseq);

fun bHead(bCons(x,_)) = x | bHead bNil = raise EmptySeq;
fun bForward(bCons(_,xf)) = xf(Forward) | bForward bNil = raise EmptySeq;
fun bBack(bCons(_,xf)) = xf(Back) | bBack bNil = raise EmptySeq;

(* 1 *)
fun intbseq k = bCons(k, fn dir => if dir = Back then intbseq(k-1) else intbseq(k+1));

(* 2 *)
fun bmap _ bNil = bNil
	| bmap f (bCons(x, xf)) = bCons(f(x), fn dir => bmap f (xf dir));

(* 3 *)
fun bfilter _ _ bNil = bNil
	| bfilter pred d (bCons(x, xf)) =
			if pred x then bCons(x, fn dir => bfilter pred dir (xf dir))
			else bfilter pred d (xf d);

(* 4 *)
fun seq2bseq (Cons(x, xf)) (Cons(y, yf)) =
			bCons(y, fn dir => 
				if dir = Back then seq2bseq (xf()) (Cons(x, fn () => Cons(y, yf)))
				else seq2bseq (Cons(y, fn () => Cons(x, xf))) (yf()))
	| seq2bseq _ _ = bNil