use "mlisp.sml";

(* testing "+" *)
print("testing +\n");
val (res,env) = eval (parse (tokenize "(+ 2 3)")) (emptyNestedEnv ());
val (res,env) = (eval (parse (tokenize "(+ (+ 1 1) 3)")) (emptyNestedEnv ()));

(* testing "-" *)
print("testing -\n");
val (res,env) = eval (parse (tokenize "(- 2 3)")) (emptyNestedEnv ());

(* testing "*" *)
print("testing *\n");
val (res,env) = eval (parse (tokenize "(* 2 3)")) (emptyNestedEnv ());

(* testing "div" *)
print("testing div\n");
val (res,env) = eval (parse (tokenize "(div 10 2)")) (emptyNestedEnv ());

(* testing "cons" *)
print("testing cons\n");
val (res,env) = eval (parse (tokenize "(cons 1 2)")) (emptyNestedEnv ());
res = CONS (ATOM (NUMBER 1),ATOM (NUMBER 2));
val (res,env) = eval (parse (tokenize "(cons 1 (cons 2 3))")) (emptyNestedEnv ());
res = CONS (ATOM (NUMBER 1),CONS (ATOM (NUMBER 2),ATOM (NUMBER 3)));
val (res,env) = eval (parse (tokenize "(cons 1 (cons 2 (cons 3 7)))")) (emptyNestedEnv ());
res = CONS (ATOM (NUMBER 1),CONS (ATOM (NUMBER 2),CONS (ATOM (NUMBER 3),ATOM (NUMBER 7))));

(* testing "car" *)
print("testing car\n");
val (res,env) = eval (parse (tokenize "(car (cons 1 (cons 2 7)))")) (emptyNestedEnv ());

(* testing "cdr" *)
print("testing cdr\n");
val (res,env) = eval (parse (tokenize "(cdr (cons 1 (cons 2 7)))")) (emptyNestedEnv ());

(* testing "define" *)
print("testing define\n");
val (res,env) = (eval (parse (tokenize "(define pi 3)")) env);
find "pi" env;