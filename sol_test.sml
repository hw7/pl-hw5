use "sol.sml";

(* 1 *)
print("Testing intbseq()\n");
intbseq 2;
bForward(it);
bForward(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);

(* 2 *)
print("Testing bmap()\n");
bmap (fn x =>x*x) (intbseq 2);
bForward(it);
bForward(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);
bmap (fn x=> x + 1) bNil;

(* 3 *)
print("Testing bfilter()\n");
bfilter (fn x => x mod 2 = 0) Back (intbseq 2);
bForward(it);
bForward(it);
bBack(it);
bBack(it);
bBack(it);
bBack(it);
bfilter (fn x => x mod 2 = 0) Back (intbseq 1);
bfilter (fn x => x mod 2 = 0) Forward (intbseq 1);
bfilter (fn x=> x > 3) Back bNil;

(* 4 *)
print("Testing seq2bseq()\n");
fun from(x) = Cons(x, fn()=>from(x+1));
fun downfrom(x) = Cons(x, fn()=>downfrom(x-1));
seq2bseq (downfrom ~1) (from 0);
bForward(it);
bBack(it);
bBack(it);
